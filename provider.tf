provider "aws" {
  profile = var.profile
  region  = var.region
}


terraform {

  required_providers {
    aws = {
      source = "hashicorp/aws"
    }
  }

  backend "s3" {
    bucket         = "terraform-state-us-east-1-054282934833"
    dynamodb_table = "dynamodb-terraform-locks-us-east-1"
    encrypt        = "true"
    key            = "aws-terraform-challenge.tfstate"
    profile        = "aws-test"
    region         = "us-east-1"
  }
}

data "aws_availability_zones" "available" {
}

provider "http" {
}
