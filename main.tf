# module "eks" {
#     source              = ".//modules/eks"
#     name                = var.name
#     cluster-role_arn    = module.iam.cluster-role_arn
#     fargate-role_arn    = module.iam.fargate-role_arn
#     nodegroup-role_arn  = module.iam.nodegroup-role_arn
#     eks_version         = var.eks_version
#     tags                = var.tags
#     pub-subnet_ids      = var.pub-subnet_ids
#     priv-subnet_ids     = var.priv-subnet_ids
#     namespaces          = var.namespaces
# }

resource "aws_dynamodb_table" "state_locking" {
  hash_key = "LockID"
  name     = "dynamodb-terraform-locks-us-east-1"
  attribute {
    name = "LockID"
    type = "S"
  }
  billing_mode = "PAY_PER_REQUEST"
}

# resource "aws_instance" "ec2_example" {
#     ami = "ami-0cff7528ff583bf9a"
#     instance_type = "t2.micro"
#     tags = {
#       Name = "EC2 Instance"
#     }
# }
