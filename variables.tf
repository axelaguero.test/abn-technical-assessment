variable "profile" {
  default = "aws-test"
  type    = string
}

variable "region" {
  default = "us-east-1"
  type    = string
}

variable "cluster-name" {
  default = "terraform-eks-challenge"
  type    = string
}